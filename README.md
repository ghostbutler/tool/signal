Signal
======

Introduction
------------

Signal manager, monitor for SIGHUP/SIGINT/SIGTERM/SIGQUIT for clean
stop.

Example
-------

To use this in your program, define an %isRunning% variable, set to true
at startup, and use it as such:

```go
// declare isRunning
isRunning := true

// listen for signal
signal.Listen( &isRunning )

// wait for service
for isRunning {
	// ... do your stuff here, it will stop loop when signal is caught
}
```

Author
------

Ghost Butler <lucas.soares.npro@gmail.com>

https://gitlab.com/ghostbutler/tool/signal.git
