package signal

import(
	"os"
	"os/signal"
	"syscall"
)

// listen will listen for all kind of termination signal, and will notify
// end user of any caught signal by changing the %isRunning% to false
func Listen( isRunning *bool ) {
	// create signal instance
	sigc := make( chan os.Signal,
		1 )

	// record catch of SIGHUP/SIGINT/SIGTERM/SIGQUIT
	signal.Notify( sigc,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT )

	// start the threaded blocking listening function for signal
	go func( ) {
		for {
			<-sigc
			*isRunning = false
		}
	}( )
}

